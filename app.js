const express = require('express');
const cors = require('cors')

const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}))

// Require all routes
const expenseRoutes = require('./src/expense/routes')
// const incomeRoutes = require('./src/income/routes')

// List of Routes
app.use('/expense', expenseRoutes);
// app.use('/income', incomeRoutes);


app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})